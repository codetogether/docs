# Code Together API Documentation

This repository specifies the API which all server and client implementations
must follow.

## Authorization

Before any interaction with the server, the client must authorize for the server
to accept connections from the client.

First, the client makes a GET request to `/authorization_information`.
The server must respond with the 200 OK status code and a JSON body of
the following schema:

| Field                  | Type    | Description                                                                        |
| ---------------------- | ------- | ---------------------------------------------------------------------------------- |
| `is_password_required` | boolean | Specifies if the server requires a password in order for the user to be authorized |

Then, the client makes a POST request to `/authorize` with a JSON body of the
following schema:

| Field      | Type    | Description                        |
| ---------- | ------- | ---------------------------------- |
| `password` | string? | A string required to be authorized |

Interpretation of the `password` field is left to the server. If the client
has not been authorized, the server responds with the `401 Unauthorized` status
code. Otherwise, the server returns a JSON body of the following schema:

| Field   | Type    | Description                                |
| ------- | ------- | ------------------------------------------ |
| `token` | string? | An optional token that identifies the user |

The server is allowed to remember the client based on its heuristics. The token,
however, may be used to indentify the user if these heuristics may fail.

If the `token` is specified, the client must pass it in the `Authorization`
header with `CodeTogether` as the authorization type.

> Example: a server running on a local network may trust authorization requests
> and may not need a password; it also can identify users based on their
> IP addresses. A public server, however, cannot trust any request, therefore
> requiring authorization with a password and identification with a token.

Whenever the server receives a connection (except the requests specified above)
and it can't identify the user, it closes the connection with
the `401 Unauthorized` status code.
